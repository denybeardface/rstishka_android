package com.danone.rastishka

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Button
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import android.Manifest
import android.app.Activity
import android.arch.persistence.room.Room
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.net.Uri
import android.os.AsyncTask
import android.os.Environment
import android.support.constraint.ConstraintLayout
import android.support.v4.content.FileProvider
import android.support.v4.content.LocalBroadcastManager
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.android.volley.RequestQueue
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.zappar.ZapparEmbed
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import android.util.Base64
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.config.AWSConfiguration
import com.amazonaws.mobileconnectors.pinpoint.PinpointConfiguration
import com.amazonaws.mobileconnectors.pinpoint.PinpointManager

class MainActivity : AppCompatActivity() {

    private lateinit var photoBtn: Button
    private lateinit var parkBtn: Button
    private lateinit var shootBtn: Button
    private lateinit var raceBtn: Button
    private lateinit var fearBtn: Button
    private lateinit var selfieBtn: Button
    private lateinit var helpBtn: Button
    private lateinit var submitBtn: Button
    private lateinit var bg: ImageView
    private lateinit var shootTtp: ImageView
    private lateinit var raceTtp: ImageView
    private lateinit var fearTtp: ImageView
    private lateinit var selfieTtp: ImageView
    private lateinit var queue: RequestQueue
    private val url = "https://api.clarifai.com/v2/searches"
    private val REQUEST_IMAGE_CAPTURE = 1
    private var mCurrentPhotoPath: String? = null
    private var helpAnimating = false
    private var ARErrorMsg = "Ваше устройство не поддерживает технологию дополненной реальности"
    private var mBroadcastReceiver: BroadcastReceiver? = null
    private var zapparIntent: Intent? = null
    private var mp: MediaPlayer? = null
    private var currentAnimatedBubble: ImageView? =  null
    private var credentialsProvider: AWSCredentialsProvider? = null
    private var awsConfiguration: AWSConfiguration? = null
    private var launchCodes = mutableListOf<Int>()
    private var counter = 0
    private var appLinkIntent: Intent? = null

    companion object {
        var database: AppDatabase? = null
        var pinpointManager: PinpointManager? = null
    }

    fun logEvent(msg: String) {
        pinpointManager?.analyticsClient?.let {
            val event = it.createEvent(msg)
            it.recordEvent(event)
            it.submitEvents()
        }
    }

    fun getLaunchCode(): Int {
        counter += 1
        return counter
    }

    fun clSearch(image: Bitmap){
        val codesAdapter = database?.listCodesDao()
        val gamesAdapter = database?.listGamesDao()

        val b64 = image2b64(image)

        val mImgContent = JSONObject()
        val mImg = JSONObject()
        val mData = JSONObject()
        val mInput = JSONObject()
        val mOutput = JSONObject()
        val outputs = JSONArray()
        val mAnds = JSONObject()
        val mQuery = JSONObject()

        mImgContent.put("base64", b64)
        mImg.put("image", mImgContent)
        mData.put("data", mImg)
        mInput.put("input",mData)
        mOutput.put("output", mInput)
        outputs.put(mOutput)
        mAnds.put("ands", outputs)
        mQuery.put("query", mAnds)

        val jsonObjRequest = object : JsonObjectRequest(Method.POST, url, mQuery,
            Response.Listener { response ->
                Log.d("Clarifai","$response")
                val data = JSONObject(response.toString())

                val hits = data.getJSONArray("hits")

                var highest: JSONObject? = null

                for (i in 0..(hits.length() - 1)){
                    val hit = hits.getJSONObject(i)
                    if(hit.getJSONObject("input").getJSONObject("data").getJSONArray("concepts").getJSONObject(0).get("name") == "package"){

                        if(highest == null){
                            highest = hit
                        } else {
                            val curHighest = highest.getDouble("score")
                            val nextPossibleHighest = hit.getDouble("score")

                            if(curHighest < nextPossibleHighest) {
                                highest = hit
                            }
                        }
                    }
                }

                if(highest != null && highest.getDouble("score") < .75) {
                    logEvent("Число неудачно обработанных фото")
                    Toast.makeText(this, "Упс! Что-то пошло не так. Попробуй ещё раз!", Toast.LENGTH_LONG).show()
                } else {
                    Log.d("RECOGNITION", "$highest")


                    AsyncTask.execute {

                        val codes = codesAdapter?.getAll()
                        val recName = highest?.getJSONObject("input")?.getJSONObject("data")?.getJSONObject("metadata")?.get("name") as String

                        if(codes?.size == 0){
                            val newCode = Code(recName)
                            val newGame = Game(recName)
                            codesAdapter?.insertAll(newCode)
                            gamesAdapter?.insertAll(newGame)
                            logEvent("Число успешно обработанных фото")
                            activateGames()
                        } else if(codes != null && codes.size < 4 ) {
                            val isDuplicate = codes.filter {
                                it.name == recName
                            }

                            if( isDuplicate.size == 0 ) {
                                val newCode = Code(recName)
                                val newGame = Game(recName)
                                codesAdapter.insertAll(newCode)
                                gamesAdapter?.insertAll(newGame)
                                logEvent("Число успешно обработанных фото")
                                activateGames()
                            } else {
                                runOnUiThread {
                                    Toast.makeText(
                                        this,
                                        "Упс! Ранее ты уже делал фото с этим героем. Сфотографируй другого и получи титул Короля Парка!",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                                Log.d("CODES", "code already activated")
                            }
                        }

                    }
                }

            },
            Response.ErrorListener {
                runOnUiThread {
                    Toast.makeText(this, "Упс! Что-то пошло не так. Попробуй ещё раз!", Toast.LENGTH_LONG).show()
                }
            }
        ){

            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/json"
                headers["Authorization"] = "Key 69b6426b5b5d4e0e805241af57e4bf57"
                return headers
            }
        }


        queue.add(jsonObjRequest)
    }

    fun searchImage(image: Bitmap) {
        val codesAdapter = database?.listCodesDao()

        AsyncTask.execute {
            val codes = codesAdapter?.getAll()

            if(codes != null && codes.size < 4) {
                clSearch(image)
            } else {
                runOnUiThread {
                    Toast.makeText(this, "Все игры разблокированы!", Toast.LENGTH_LONG).show()
                }
            }
        }


    }

    fun fadeIn(view: View) {
        if(!helpAnimating){
            helpAnimating = true
            view.animate().alpha(1f).setDuration(400).setStartDelay(0L).withEndAction {
                helpAnimating = false
            }
        } else {
            return
        }
    }

    fun fadeInOut(view: View) {
        view.animate().alpha(1f).setDuration(400).setStartDelay(0L).withEndAction {
            println("fading out")
            view.animate().alpha(0f).setDuration(400).setStartDelay(5000L)
        }
    }

    fun fadeOutImmediate(view: View) {
            view.animate().alpha(0f).setDuration(400).setStartDelay(0L)
    }

    fun image2b64(bitmap: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val bytes = baos.toByteArray()

        val temp = Base64.encodeToString(bytes, Base64.DEFAULT)

        Log.d("base64", temp)
        return temp
    }

    private fun takePicture() {

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file: File = createFile()

        val uri: Uri = FileProvider.getUriForFile(
            this,
            "com.example.android.fileprovider",
            file
        )
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

    @Throws(IOException::class)
    private fun createFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_",
            ".jpg",
            storageDir
        ).apply {
            mCurrentPhotoPath = absolutePath
        }
    }

    private fun methodWithPermissions() = runWithPermissions(Manifest.permission.CAMERA, Manifest.permission.INTERNET) {
        takePicture()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {

            val bitmap: Bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath)
            val scaledBitmap = Bitmap.createScaledBitmap(
                bitmap,
                240,
                480,
                false
            )

            searchImage(scaledBitmap)
        }
    }

    fun activateGames() {
        val gamesAdapter = database?.listGamesDao()
        var gamesCount: Int?
        val isCompatible = ZapparEmbed.isCompatible(this)

        AsyncTask.execute {
            gamesCount = gamesAdapter?.getAll()?.size

            Log.d("GameActivation", "$gamesCount")

            runOnUiThread {
                when(gamesCount) {
                    0 -> println("no games")
                    1 -> {
                        shootBtn.setBackgroundResource(R.drawable.attr2)
                    }
                    2 -> {
                        shootBtn.setBackgroundResource(R.drawable.attr2)
                        raceBtn.setBackgroundResource(R.drawable.attr3)
                    }
                    3 -> {
                        shootBtn.setBackgroundResource(R.drawable.attr2)
                        raceBtn.setBackgroundResource(R.drawable.attr3)
                        fearBtn.setBackgroundResource(R.drawable.attr4)
                    }
                    4 -> {
                        shootBtn.setBackgroundResource(R.drawable.attr2)
                        raceBtn.setBackgroundResource(R.drawable.attr3)
                        fearBtn.setBackgroundResource(R.drawable.attr4)
                        selfieBtn.setBackgroundResource(R.drawable.attr5)
                    }
                    else -> println("probably an error happened")
                }

                parkBtn.setOnClickListener {
                    logEvent("Клик на иконку «Парк аттракционов»")
                    if(isCompatible) {
                        zapparIntent = Intent(this, ZapparEmbed.getZapcodeClassForIntent())
                        zapparIntent?.putExtra(ZapparEmbed.EXTRA_LAUNCH_DEEP_LINK, "z/B3Sk1c")
                        val launchCode = getLaunchCode()
                        launchCodes.add(launchCode)
                        startActivityForResult(zapparIntent, launchCode)
                    } else {
                        Toast.makeText(this, ARErrorMsg, Toast.LENGTH_LONG).show()
                    }
                }

                shootBtn.setOnClickListener {
                    logEvent("Клик на иконку «Водный тир»")

                    if(isCompatible) {
                        if(gamesCount as Int >= 1){
                            zapparIntent = Intent(this, ZapparEmbed.getZapcodeClassForIntent())
                            zapparIntent?.putExtra(ZapparEmbed.EXTRA_LAUNCH_DEEP_LINK, "z/ADjn1c")
                            val launchCode = getLaunchCode()
                            launchCodes.add(launchCode)
                            startActivityForResult(zapparIntent, launchCode)
                        } else {
                            logEvent("Показалась всплывашка «Сфотографируй палочку»")

                            if(currentAnimatedBubble != null){
                                currentAnimatedBubble?.clearAnimation()
                                fadeOutImmediate(currentAnimatedBubble as View)
                            }

                            currentAnimatedBubble = shootTtp
                            fadeInOut(shootTtp)

                        }

                    } else {
                        Toast.makeText(this, ARErrorMsg, Toast.LENGTH_LONG).show()
                    }
                }

                raceBtn.setOnClickListener {
                    logEvent("Клик на иконку «Гонки»")                    

                    if(isCompatible) {
                        if(gamesCount as Int >= 2){
                            val openURL = Intent(android.content.Intent.ACTION_VIEW)
                            openURL.data = Uri.parse("https://brainrus.ru/rstishkaRace")
                            val launchCode = getLaunchCode()
                            launchCodes.add(launchCode)
                            startActivityForResult(openURL, launchCode)
                        } else {
                            logEvent("Показалась всплывашка «Сфотографируй палочку»")

                            if(currentAnimatedBubble != null){
                                currentAnimatedBubble?.clearAnimation()
                                fadeOutImmediate(currentAnimatedBubble as View)
                            }

                            currentAnimatedBubble = raceTtp
                            fadeInOut(raceTtp)

                        }

                    } else {
                        Toast.makeText(this, ARErrorMsg, Toast.LENGTH_LONG).show()
                    }

                }

                fearBtn.setOnClickListener {
                    logEvent("Клик на иконку «Комната страха»")

                    if(isCompatible) {
                        if(gamesCount as Int >= 3){
                            zapparIntent = Intent(this, ZapparEmbed.getZapcodeClassForIntent())
                            zapparIntent?.putExtra(ZapparEmbed.EXTRA_LAUNCH_DEEP_LINK, "z/5BNo1c")
                            val launchCode = getLaunchCode()
                            launchCodes.add(launchCode)
                            startActivityForResult(zapparIntent, launchCode)
                        } else {
                            logEvent("Показалась всплывашка «Сфотографируй палочку»")

                            if(currentAnimatedBubble != null){
                                currentAnimatedBubble?.clearAnimation()
                                fadeOutImmediate(currentAnimatedBubble as View)
                            }

                            currentAnimatedBubble = fearTtp
                            fadeInOut(fearTtp)

                        }

                    } else {
                        Toast.makeText(this, ARErrorMsg, Toast.LENGTH_LONG).show()
                    }

                    
                }

                selfieBtn.setOnClickListener {
                    logEvent("Клик на иконку «Селфи короля парка»")

                    if(isCompatible) {
                        if(gamesCount as Int >= 4){
                            zapparIntent = Intent(this, ZapparEmbed.getZapcodeClassForIntent())
                            zapparIntent?.putExtra(ZapparEmbed.EXTRA_LAUNCH_DEEP_LINK, "z/7UTn1c")
                            val launchCode = getLaunchCode()
                            launchCodes.add(launchCode)
                            startActivityForResult(zapparIntent, launchCode)
                        } else {
                            logEvent("Показалась всплывашка «Сфотографируй палочку»")

                            if(currentAnimatedBubble != null){
                                currentAnimatedBubble?.clearAnimation()
                                fadeOutImmediate(currentAnimatedBubble as View)
                            }

                            currentAnimatedBubble = selfieTtp
                            fadeInOut(selfieTtp)

                        }

                    } else {
                        Toast.makeText(this, ARErrorMsg, Toast.LENGTH_LONG).show()
                    }

                    
                }
            }

        }


    }

    override fun onRestart() {
        super.onRestart()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()

        Log.d("ActionBar", (actionBar == null).toString())

        val ctx = this
        mp = MediaPlayer.create(this, R.raw.help)

        try {
            database = Room.databaseBuilder(this, AppDatabase::class.java,
                "rstishka-db").build()
        } catch (e: Exception) {
            Log.i("setup", e.message)
        }
        AWSMobileClient.getInstance().initialize(this).execute()

        with (AWSMobileClient.getInstance()) {
            val config = PinpointConfiguration(ctx, credentialsProvider, configuration)
            pinpointManager = PinpointManager(config)
        }

        pinpointManager?.sessionClient?.startSession()
        pinpointManager?.analyticsClient?.submitEvents()

        val filter = IntentFilter(ZapparEmbed.ACTION_HOST_MESSAGE)
        mBroadcastReceiver = object: BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                val msg = intent?.getStringExtra(ZapparEmbed.EXTRA_HOST_MESSAGE)
                Log.d("Zap2App", msg);
//                Toast.makeText(ctx,msg, Toast.LENGTH_LONG).show()
                if(msg!!.contains("finish")){
                    Log.d("CODES", launchCodes.toString())
                    launchCodes.forEach {
                        code ->
                        Log.d("CODE",code.toString())
                        finishActivity(code)
                        AsyncTask.execute{
                            val gamesAdapter = database?.listGamesDao()
                            val gamesCount = gamesAdapter?.getAll()?.size
                            Log.d("GAMESCOUNT", gamesCount.toString())
                            if(gamesCount == 0 ){
                                runOnUiThread {
                                    val helpView = findViewById<ConstraintLayout>(R.id.helpViewContainer)
                                    fadeIn(helpView)
                                    mp?.start()
                                    submitBtn.isClickable = true
                                }
                            }
                        }

                    }
                    counter = 0

                } else if(msg!!.contains("launch")) {
                    val deeplink = "z/"+msg.split(":")[1]
                    zapparIntent = Intent(ctx, ZapparEmbed.getZapcodeClassForIntent())
                    zapparIntent?.putExtra(ZapparEmbed.EXTRA_LAUNCH_DEEP_LINK, deeplink)
                    val launchCode = getLaunchCode()
                    launchCodes.add(launchCode)
                    startActivityForResult(zapparIntent, launchCode)
                } 
            }
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver as BroadcastReceiver, filter)

        queue = Volley.newRequestQueue(this)

        photoBtn = findViewById(R.id.takePhoto)
        parkBtn = findViewById(R.id.parkBtn)
        shootBtn = findViewById(R.id.shootBtn)
        raceBtn = findViewById(R.id.raceBtn)
        fearBtn = findViewById(R.id.roomBtn)
        selfieBtn = findViewById(R.id.selfieBtn)
        helpBtn = findViewById(R.id.helpBtn)
        submitBtn = findViewById(R.id.submitBtn)
        bg = findViewById(R.id.main_bg)
        shootTtp = findViewById(R.id.shootTtp)
        raceTtp = findViewById(R.id.raceTtp)
        fearTtp = findViewById(R.id.fearTtp)
        selfieTtp = findViewById(R.id.selfieTtp)
        val helpView = findViewById<ConstraintLayout>(R.id.helpViewContainer)


        photoBtn.setOnClickListener {
            methodWithPermissions()
            logEvent("Клик на кнопку «Сфотографируй палочку»")
        }

        helpBtn.setOnClickListener {
            fadeIn(helpView)
            helpView.isClickable = true
            submitBtn.isClickable = true
            mp?.start()
        }

        bg.setOnClickListener {
            if(mp?.isPlaying == true){
                mp?.pause()
                mp?.seekTo(0)
            }
            helpView.isClickable = false
            submitBtn.isClickable = false
            fadeOutImmediate(helpView)
        }

        submitBtn.setOnClickListener {
            if(mp?.isPlaying == true){
                mp?.pause()
                mp?.seekTo(0)
            }
            helpView.isClickable = false
            submitBtn.isClickable = false
            fadeOutImmediate(helpView)
        }

        submitBtn.isClickable = false

        // DB update for games overflow
        AsyncTask.execute{
            val gamesAdapter = database?.listGamesDao()
            val codesAdapter = database?.listCodesDao()
            val codes = codesAdapter?.getAll()
            val games = gamesAdapter?.getAll()

            if(games != null && codes != null && codes.size > 4 ){
               val remCodes = codes.subList(3, codes.size - 1)
               val remGames = games.subList(3, games.size - 1)
                for(code in remCodes){
                    codesAdapter.delete(code)
                }
                for(game in remGames) {
                    gamesAdapter.delete(game)
                }
            }

//            val code = Code("foo")
//            val game = Game("foo")
//
//            codesAdapter?.insertAll(code)
//            gamesAdapter?.insertAll(game)
        }

        activateGames()

    }

}
