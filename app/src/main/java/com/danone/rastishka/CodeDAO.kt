package com.danone.rastishka

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface ListCodesDao {

    @Query("SELECT * FROM codes")
    fun getAll(): List<Code>

    @Insert
    fun insertAll(vararg codes: Code)

    @Delete
    fun delete(code: Code)
}