package com.danone.rastishka

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface ListGamesDao {

    @Query("SELECT * FROM games")
    fun getAll(): List<Game>

    @Insert
    fun insertAll(vararg games: Game)

    @Delete
    fun delete(game: Game)
}