package com.danone.rastishka

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "codes")
data class Code(@ColumnInfo(name="name") var name: String,
                        @ColumnInfo(name="id") @PrimaryKey(autoGenerate = true) var id: Long = 0)