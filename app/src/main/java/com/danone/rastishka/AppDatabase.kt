package com.danone.rastishka

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [(Game::class), (Code::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun listGamesDao(): ListGamesDao
    abstract fun listCodesDao(): ListCodesDao
}